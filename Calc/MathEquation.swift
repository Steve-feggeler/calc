//
//  MathEquation.swift
//  Calc
//
//  Created by stephen feggeler on 6/29/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

struct MathEquation
{
    private var equation = [String]()
    private var lastOperation = [String]()
    private var suffix = [String]()
    
    enum Constants : String {
        case π = "π"
        case φ = "φ"
        case e = "e"
        
        static let allValues = [π.rawValue, φ.rawValue, e.rawValue]
        
        var double:Double? {
            switch(self){
            case .π: return Double.pi
            case .e: return exp(1)
            case .φ: return 1.61803398874
            }
        }
    }
    
    enum AngleType {
        case degrees, radians
        
        mutating func next() {
            switch self {
            case .degrees: self = .radians
            case .radians: self = .degrees
            }
        }
    }
    
    var maxDigits:Int = 12
    var maxFractionDigits:Int = 11
    var angleType = AngleType.degrees
    
    let operators:[String:Operator] = {
        
        return [
            Operator(symbol: "max", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "sin", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "cos", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "tan", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "sinh", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "cosh", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "tanh", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "asin", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "acos", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "atan", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "asinh", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "acosh", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "atanh", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "log2", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "log", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "ln", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "√", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "∛", precedence: 115, associativity: .left, isFunction: true),
            Operator(symbol: "nthRoot", precedence: 110, associativity: .right),
            Operator(symbol: "^", precedence: 110, associativity: .right),
            Operator(symbol: "✕", precedence: 100, associativity: .left),
            Operator(symbol: "×", precedence: 100, associativity: .left),
            Operator(symbol: "/", precedence: 100, associativity: .left),
            Operator(symbol: "÷", precedence: 100, associativity: .left),
            Operator(symbol: "E", precedence: 115, associativity: .left),
            Operator(symbol: "!", precedence: 115, associativity: .left, unary: true),
            Operator(symbol: "%", precedence: 115, associativity: .left, unary: true),
            Operator(symbol: "+or-%", precedence: 95, associativity: .right, unary: true),
            Operator(symbol: "+", precedence: 90, associativity: .left ),
            Operator(symbol: "-", precedence: 90, associativity: .left )
        ].toDictionary { $0.symbol }
    }()
    
    private var startNewNumber = false
    private var isFirstIndexAnswer = false

    var suffixCount:Int {
        return suffix.count
    }
    
    var redueLastOperation:Bool {
        return equation.count == 1 &&  lastOperation.count > 0
    }    
    
    enum ClearState:String {
        case clear = "C", allClear="AC"
    }
    
    private var _clearState:ClearState = .allClear
    
    var clearState:ClearState {
        return _clearState
    }
    
    var termCount:Int {
        return equation.count
    }
    
    func lastNumber() -> Double? {
         for term in equation.reversed() {
             if term.isNumber {
                return Double(term)
             }
         }
         return nil
    }
    
    func logC(val: Double, forBase base: Double) -> Double {
        return log(val)/log(base)
        
        // print(logC(9.0, forBase: 3.0)) // "2.0"
        // print(logC(16.0, forBase: 4.0)) // "2.0"
    }
    
    mutating func append(constant:Constants) {
        
        _clearState = .clear
        startNewNumber = false
        if shouldAppendMultSymbol() {
            append(operation: "✕")
        }
        
        if equation.isLastZero && equation.count == 1 {
            equation.removeAll()
        }
        
        // if last was number(not zero), append 'x'
        // so if last was '5' then '5 x 10 ^'
        
        if equation.isLastNumber(alsoNumbers: Constants.allValues) {
            append(operation: "✕")
        }
        
        equation.append(constant.rawValue)
    }
    
    mutating func append(function:String) {
        
        _clearState = .clear
        startNewNumber = false
        
        /*
        if isChangingFunction() {
            equation.removeLast()
        }
        */
        
        if function != "!" && shouldAppendMultSymbol() {
            append(operation: "✕")
        }
        
        if equation.isLastZero && equation.count == 1 {
            equation.removeAll()
        }
        
        // if last was number(not zero), append 'x'
        // so if last was '5' then '5 x 10 ^'
        
        if function != "!" && equation.isLastNumber(alsoNumbers: Constants.allValues) {
            append(operation: "✕")
        }

        equation.append(function)
        
        self.appendLeftRoundBracket()
    }
    
    mutating func append(double:Double) {
        
        _clearState = .clear
        startNewNumber = true
        
        if shouldAppendMultSymbol() {
            append(operation: "✕")
        }
        
        let newNumberString = "\(double)".trimTrailingDotZero()
        
        let last = equation.last ?? ""
        
        let appendNewNumber = !last.isNumber(alsoNumbers: Constants.allValues) || equation.count == 0
        
        if appendNewNumber {
            
            // last was an operation like * 0r +
            
            equation.append(newNumberString)
        }
        else {
            
            // replace last value with new number
            
            equation[equation.count - 1] = newNumberString
        }
    }
    
    mutating func tenToPower() {
        
        _clearState = .clear
        startNewNumber = false
        
        if equation.isLastZero && equation.count == 1 {
            equation.removeAll()
        }
        
        // if last was number(not zero), append 'x'
        // so if last was '5' then '5 x 10 ^'
        
        if equation.isLastNumber(alsoNumbers: Constants.allValues) {
            append(operation: "✕")
        }
        
        append(double: 10)
        append(operation: "^")
    }
    
    mutating func appendLeftRoundBracket() {
        
        _clearState = .clear
        startNewNumber = false
        
        // if "0" then "("
        
        if equation.count == 1 && equation.isLastZero {
            
            equation[0] = "("
        }
        else {
            
            if equation.isLastNumber(alsoNumbers: Constants.allValues) {
                append(operation: "✕")
            }

            equation.append("(")
        }
        
        suffix.append(")")
    }
    
    private func canAppendRightRoundBracket() -> Bool {
        
        return suffix.count > 0
    }
    
    mutating func appendRightRoundBracket() {
        
        if canAppendRightRoundBracket() == false {
            return
        }
        
        _clearState = .clear
        
        startNewNumber = true
        
        equation.append(")")
        
        if suffix.count > 0 {
            suffix.removeFirst()
        }
    }
    
    func shouldAppendMultSymbol() -> Bool {
        let last = equation.last ?? ""        
        if last == ")" || Constants.allValues.contains(last) {
            return true
        }
        
        return false
    }
    
    mutating func scientificNotation() -> Bool {
        
        let last = equation.last ?? ""
        
        if !last.isNumber {
            return false
        }
        
        equation.append("E")
        
        return true
    }
    
    mutating func append(digit:String) {
        
        _clearState = .clear
        
        if shouldAppendMultSymbol() {
            append(operation: "✕")
        }
        
        // start new number or append digit
        
        var last = equation.last ?? ""
        
        let startNumber = !last.isNumber || equation.count == 0
        
        if startNumber {
            equation.append(digit)
        }
        else {
            
            if startNewNumber {
                last = ""
                startNewNumber = false
            }
            
            // change "-02" to "-2" and keep "-0.2"
            
            if last.hasPrefix("-0") && !last.hasPrefix("-0.") { last = "-" }
            
            // if already max digits, nothing to do
            
            if last.digits.count >= maxDigits {
                return
            }
            
            // if last was '0', overwrite it
            // since '02' should be '2'
            
            if last == "0" { last = "" }
            
            equation[equation.count - 1] = last + digit
        }
    }
    
    mutating func append(decimal:String) {
        
        let last = equation.last ?? ""
        
        // if already max digits, nothing to do
        
        if last.digits.count >= maxDigits {
            return
        }
        
        if startNewNumber {
            equation = ["0."]
            startNewNumber = false
            return
        }
        
        // if last not a number (ex was *,+,-)
        // then start a new number
        
        if !last.isNumber {
            equation.append("0.")
        }
            
        // else if integer number then add '.'
            
        else if last.index(of: ".") == nil {
            let newNumber = last + "."
            equation.removeLast()
            equation.append(newNumber)
        }
    }
    
    private func isChangingOperation() -> Bool {
        let last = equation.last ?? ""
        let isNumber = equation.isLastNumber(alsoNumbers: Constants.allValues)
        return !isNumber && equation.count > 0 && last != "%" && last != ")"
    }
    
    private func shouldRemovePrevForOperation() -> Bool {
        let last = equation.last ?? ""
        let isNumber = equation.isLastNumber(alsoNumbers: Constants.allValues)
        return !(isNumber || equation.count == 0 || last == "%" || last == "!" || last == ")")
    }
    
    private func isChangingFunction() -> Bool {
        let last = equation.last ?? ""
        let isNumber = equation.isLastNumber(alsoNumbers: Constants.allValues)
        if let op = operators[last], !isNumber && equation.count > 0 && op.isFunction {
            return true
        }
        return false
    }
    
    func canAppendOperation() -> Bool {
        let last = equation.last ?? ""
        if last == "(" {
            return false
        }
        return true
    }
    
    mutating func append(operation:String) {
        if canAppendOperation() == false {
            return
        }
        
        // if changing operation, (ex user types '*+', remove '*' have just '+')
        
        while shouldRemovePrevForOperation() {
            equation.removeLast()
        }
        
        if equation.count == 0 {
            equation.append("0")
        }
        
        equation.append(operation)
        startNewNumber = false
    }
    
    mutating func sign() {
        
        guard var last = equation.last else { return }
        
        // if last not a number, append "-0"
        
        if !last.isNumber {
            equation.append("-0")
            return
        }
        
        // change sign of number
        
        var newLast:String
        
        if last.first == "-" {
            last.removeFirst()
            newLast = last
        }
        else {
            newLast = "-" + last
        }
        
        // update the new number
        
        equation[equation.count - 1] = newLast
    }
    
    mutating func percent() {
        
        let last = equation.last ?? ""
        
        if !last.isNumber && equation.count > 0 && operators[last]?.unary == false {
            equation.removeLast()
        }
        
        equation.append("%")
        print("equation \(equation)")
        startNewNumber = true
    }
    
    mutating func factorial() {
        
        let last = equation.last ?? ""
        
        // if changing operation, (ex user types '*+', remove '*')
        
        if !last.isNumber && equation.count > 0 && operators[last]?.unary == false {
            equation.removeLast()
        }
        
        equation.append("!")
        print("equation \(equation)")
        startNewNumber = true
    }

    mutating func clear() {
        
        if _clearState == .allClear {
            lastOperation.removeAll()
        }
        _clearState = .allClear
        
        equation = ["0"]
        suffix = []
        
        startNewNumber = false
        isFirstIndexAnswer = false
    }
    
    mutating func equal() -> Double? {
        
        startNewNumber = true
        
        // if user repeatedly pressed equal,
        // redue the last operation
        
        /*
        if redueLastOperation {
            equation.append(contentsOf: lastOperation)
        }
        */
        
        // convert from infix to postfix notation
        
        let postfix:[String] = infixToPostfix(infix: equation)
        
        // evaluate the postfix expression
        
        var result:Double?
        
        do {
            result = try evaluate(postfix: postfix)
        }
        catch {
            print("evaluate thrown")
        }
        
        // save the last two operators to enable
        // redue on repeatedly pressing equal
        
        if equation.count >= 2 {
            lastOperation =  Array(equation.suffix(2))
        }
        
        if let answer = result {
            equation = ["\(answer)".trimTrailingDotZero()]
            isFirstIndexAnswer = true
        }
        else {
            equation = ["0"]
        }
        
        suffix = []
        
        return result
    }
    
    mutating func equal() -> String? {
        let answerDouble:Double? = equal()
        if let answer = answerDouble {
            let stringAnswer = "\(answer)".doubleFormatted(maxTotalDigits: maxDigits, maxFractionDigits: maxFractionDigits)!.trimTrailingDotZero()
            return stringAnswer
        }
        return nil
    }
    
    mutating func removeLast() {
        //print("equation.count = \(equation.count) isFirstIndexAnswer = \(isFirstIndexAnswer)")
        
        if equation.count == 1 && isFirstIndexAnswer {
            equation = ["0"]
            isFirstIndexAnswer = false
            return
        }
        
        
        // equation reduces to zero
        if equation.count == 0 {
            equation.append("0")
            isFirstIndexAnswer = false
            return
        }

        // remove last term in equation
        var last = equation.removeLast()
        
        // if removing right bracket, add to suffix so still ballanced
        if last == ")" {
            suffix.insert(")", at: 0)
        }
        
        // if finite number, remove a least significan digit
        // and append new number if there is one
        if last.isNumber {
            let theDouble = Double(last)!
            if theDouble.isFinite {
                last.removeLast()
                if last.count > 0 && String(last).isNumber {
                    equation.append(last)
                }
            }
        }
        
        // remove partner suffix
        if last == "(" && suffix.count > 0 {
            suffix.removeFirst()
        }
        
        // if new last is function, remove it
        if last == "(" {
            
            if let newLast =  equation.last {
                if let op = operators[newLast] {
                    if op.isFunction {
                        equation.removeLast()
                    }
                }
            }
        }
        
        // reset to zero
        if equation.count == 0 {
            equation.append("0")
            isFirstIndexAnswer = false
        }
    }
    
    func testFormatter() {
        
        let b1 = "100000000.001"
        let a1 = b1.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 12)
        print("formatMe before: \(b1) after: \(String(describing: a1))")
        
        let b2 = "100000000.123456789"
        let a2 = b2.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 2)
        print("formatMe before: \(b2) after: \(String(describing: a2))")
        
        let b3 = ".123456789123456789"
        let a3 = b3.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 4)
        print("formatMe before: \(b3) after: \(String(describing: a3))")
        
        let b4 = "-123456789876543.123456789123456789"
        let a4 = b4.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 4)
        print("formatMe before: \(b4) after: \(String(describing: a4))")
        
        let b5 = "0.000000000009954677464644646"
        let a5 = b5.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 4)
        print("formatMe before: \(b5) after: \(String(describing: a5))")
        
        let b6 = "0.0000"
        let a6 = b6.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 4)
        print("formatMe before: \(b6) after: \(String(describing: a6))")
        
        let b7 = "1.2345678987654e4"
        let a7 = b7.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 4)
        print("formatMe before: \(b7) after: \(String(describing: a7))")
        
        let b8 = "5."
        let a8 = b8.doubleFormatted(maxTotalDigits: 12, maxFractionDigits: 4)
        print("formatMe before: \(b8) after: \(String(describing: a8))")
    }
    
    func runningTotal() -> String? {
        
        var result:Double?
        var input = equation + suffix
        
        // attempt to evaluate the entire expression
        // if evaluating throws an exception, keep removing
        // last term until it evaluates, or return nil
        
        repeat {
            let postfix = infixToPostfix(infix: input, runningTotal: false)
        
            do {
                result = try evaluate(postfix: postfix)
                break
            }
            catch {
                // if last operand is a binary type,
                // can't evaluate '2+' so just return
                // nil
                if input.count == 2 {
                    break
                }
                input.removeLast()
            }
            
        } while input.count > 0
        
        if let result = result {
            
            return "\(result)".doubleFormatted(maxTotalDigits: maxDigits, maxFractionDigits: maxFractionDigits)!.trimTrailingDotZero()
            
            //return "\(result)".doubleValueFormatted(maxTotalDigits: maxDigits, maxFractionDigits: maxFractionDigits)?.trimTrailingDotZero()
            
            //return result.calculatorStyle2.trimTrailingDotZero()
        }
        
        return nil
    }
    
    func joinedAndFormatted() -> String {
        
        // This method keeps the extra zeros after the decimal point.
        // Format the integer part with commmas, and
        // leave the trailing zeros (ex: 55345.4000 -> 55,345.4000)
        
        var total:String = ""
        
        for op in self.equation {
            
            // format the integer part with commmas
            // leave the trailing zeros (ex: 55,345.4000)
            // This is needed when user is typing in numbers
            // and enters ex: 345004.0003 gets formated into
            // 345,004.0003
            
            if op.isNumber(alsoNumbers: Constants.allValues) {
                
                if Constants.allValues.contains(op) {
                    total += "\(op)"
                }
                else {
                
                    total += op.doubleFormatted(maxTotalDigits: maxDigits, maxFractionDigits: maxFractionDigits)!
                }
            }
                
            // operation (ex: *,+ ,-,...)
                
            else {
                
                total += "\(op)"
            }
        }
        
        for op in suffix {
            
            total += op
        }
    
        total = total.replacingOccurrences(of: "/", with: "÷")
        total = total.replacingOccurrences(of: "✕", with: "×")
        
        return total
    }
    
    func joinedAndFormatted(font:UIFont, textColor:UIColor, minimumScaleFactor:CGFloat, minContainerSize:CGSize) -> NSMutableAttributedString {
        
        let bigFont = font
        let smallFont = bigFont.withSize(bigFont.pointSize * minimumScaleFactor)
        let bigAttributedText = self.joinedAndFormatted(font: bigFont, textColor: textColor)
        
        if minimumScaleFactor == 0 {
            return bigAttributedText
        }
        
        let bigTextWidth = bigAttributedText.width(withConstrainedHeight: minContainerSize.height)
        
        // if big font text fits, return big text
        
        if bigTextWidth <= minContainerSize.width {
            return bigAttributedText
        }
        
        // shrink font, but not smaller then smallFont
           
        /*
        // another method for
        var currentFont = font
        let originalFontSize = currentFont.pointSize
        
        var currentText = joinedAndFormatted(font: currentFont, textColor: textColor)
        var currentSize = currentText.size()
        
        while currentSize.width > minContainerSize.width && currentFont.pointSize > (originalFontSize * minimumScaleFactor) {
            currentFont = currentFont.withSize(currentFont.pointSize - 0.5)
            currentText = joinedAndFormatted(font: currentFont, textColor: textColor)
            currentSize = currentText.size()
        }
        
        //currentSize.width = currentSize.width.round(nearest: 0.5)
        //currentSize.height = currentSize.height.round(nearest: 0.5)
        
        return currentText
        */
        
        let adjustedPointSize = max (bigFont.pointSize * (minContainerSize.width - 10) / bigTextWidth, smallFont.pointSize )
        
        let newFont = smallFont.withSize(adjustedPointSize)
        let newAttrbText = self.joinedAndFormatted(font: newFont, textColor: textColor)
        
        //let dWidth = newAttrbText.width(withConstrainedHeight: minSize.height) - minSize.width        
        //print("delta width: \(dWidth)")
        
        return newAttrbText
    }
    
    func joinedAndFormatted(font:UIFont, textColor:UIColor) -> NSMutableAttributedString {

        var braceStack = Stack<Int>()
        var dotBoxLevel = 0
        let mdas = CharacterSet(charactersIn: "+-/✕")
        let mdas2 = CharacterSet(charactersIn: "+-÷×")
        var attribStrings = [NSMutableAttributedString]()
        
        // This method keeps the extra zeros after the decimal point.
        // Format the integer part with commmas, and
        // leave the trailing zeros (ex: 55345.4000 -> 55,345.4000)
        
        var level = 0
        
        for (index, op) in self.equation.enumerated() {
            
            // increase level
            if op == "^" {
                level += 1
            }
                
            if op == "(" {
                braceStack.push(level)
            }
            else if op == ")" {
                level = braceStack.pop()
            }
            
            // decrease level (number can be negative)
            if op.rangeOfCharacter(from: mdas) != nil && op.count == 1 {
                if let currentBraceLevel = braceStack.peek() {
                    level = currentBraceLevel
                }
                else {
                    level = 0
                }
            }
            
            // format the integer part with commmas
            // leave the trailing zeros (ex: 55,345.4000)
            // This is needed when user is typing in numbers
            // and enters ex: 345004.0003 gets formated into
            // 345,004.0003
            
            if op.isNumber {                
                let numberString = op.doubleFormatted(maxTotalDigits: maxDigits, maxFractionDigits: maxFractionDigits)!
                let attributes = textAttribute(font: font, textColor: textColor, level: level, subLevel: false)
                let part = NSMutableAttributedString(string: numberString, attributes: attributes)
                attribStrings.append(part)
            }
            else if Constants.allValues.contains(op) {
                let attributes = textAttribute(font: font, textColor: textColor, level: level, subLevel: false)
                let part = NSMutableAttributedString(string: op, attributes: attributes)
                attribStrings.append(part)
            }
                
            // operation (ex: *,+ ,-,...)
                
            else {
                var opAdjusted = op.replacingOccurrences(of: "/", with: "÷")
                opAdjusted = opAdjusted.replacingOccurrences(of: "✕", with: "×")
                
                let attributes = textAttribute(font: font, textColor: textColor, level: level, subLevel: false)
                var part = NSMutableAttributedString(string: opAdjusted, attributes: attributes)
                
                if opAdjusted == "log2" {
                    
                    let pt = NSMutableAttributedString()
              
                    let attributes = textAttribute(font: font, textColor: textColor, level: level, subLevel: false)
                    let subAttrib = textAttribute(font: font, textColor: textColor, level: level, subLevel: true)
                    
                    let p1 = NSMutableAttributedString(string: "log", attributes: attributes)
                    let p2 = NSMutableAttributedString(string: "2", attributes: subAttrib)
                    
                    pt.append(p1)
                    pt.append(p2)
                    
                    part = pt
                }
                else if opAdjusted == "log" {
                    
                    let pt = NSMutableAttributedString()
                    
                    let attributes = textAttribute(font: font, textColor: textColor, level: level, subLevel: false)
                    let subAttrib = textAttribute(font: font, textColor: textColor, level: level, subLevel: true)

                    let p1 = NSMutableAttributedString(string: "log", attributes: attributes)
                    let p2 = NSMutableAttributedString(string: "10", attributes: subAttrib)
                    
                    pt.append(p1)
                    pt.append(p2)
                    
                    part = pt
                }
                else if opAdjusted == "^" {
                    var attributes = textAttribute(font: font, textColor: textColor, level: level)
                    if index == equation.count - 1 {
                        attributes[NSAttributedStringKey(rawValue: "PlaceHolder")] = true
                    }
                    part = NSMutableAttributedString(string: "^", attributes: attributes)
                }
                else if opAdjusted == "nthRoot" {
                    let attributes = textAttribute(font: font, textColor: textColor, level: level)
                    part = NSMutableAttributedString(string: "nthRoot", attributes: attributes) // ؇
                }
                
                // add space around '+, -, ÷, x'
                
                if opAdjusted.rangeOfCharacter(from: mdas2) != nil && opAdjusted.count == 1 {

                    let attributes = textAttribute(font: font, textColor: #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), level: level)
                    
                    part = NSMutableAttributedString(string: opAdjusted, attributes: attributes)
                    attribStrings.append(part)
                }
                else {
                    // functions, factorial, percent, ...
                    attribStrings.append(part)
                }
            }
            
            if op == "nthRoot" {
                level += 1
                dotBoxLevel = level
            }
        }
        
        for (_, op) in suffix.enumerated() {
            
            var level = 0
            if braceStack.count > 0 {
                level = braceStack.pop()
            }
            
            let suffixAttrib = textAttribute(font: font, textColor: UIColor.lightGray, level: level)
            let part = NSMutableAttributedString(string: op, attributes: suffixAttrib)
            
            attribStrings.append(part)
        }
        
        let dotBoxAttrib = textAttribute(font: font, textColor: textColor, level: dotBoxLevel)
        let dotBoxAttribText = NSMutableAttributedString(string: "⬚", attributes: dotBoxAttrib)
        
        var reordered = reorderNthRoot(input:attribStrings, placeHolder:dotBoxAttribText)
        
        reordered = removeCaret(input: reordered, placeHolder: "⬚")
        
        reordered = addSpaceBetween(charSet: mdas2, arrayOfAttributedStrings: reordered)
        
        //reordered = centerBaselineOffset(attributedStrings: reordered)
        
        var totalAttribString = NSMutableAttributedString()
        for attribString in reordered {
            totalAttribString.append(attribString)
        }
        
        totalAttribString = increaseLineSpacing(attributedString: totalAttribString, lineHeightMultiple: 1.2)
        
        //testReorder()
        
        return totalAttribString
    }
    
    func centerBaselineOffset(attributedStrings:[NSMutableAttributedString]) -> [NSMutableAttributedString] {
        
        var minOffset:CGFloat = 0.0
        var maxOffset:CGFloat = 0.0
        for ( _ , attribString) in attributedStrings.enumerated() {
            //let string = attribString.string
            for k in 0..<attribString.length {
                let attribs = attribString.attributes(at: k, longestEffectiveRange: nil, in: NSRange(location: 0, length: attribString.length))
                    //print("string: \(string) i: \(i)")
                if let offset = attribs[.baselineOffset] as? CGFloat {
                    minOffset = min(minOffset, offset)
                    maxOffset = max(maxOffset, offset)
                    //print("****** offset is \(offset)")
                }
            }
        }
        
        if  (minOffset < 0) || ((maxOffset + minOffset) < 27)  {
            return attributedStrings
        }
        
        let reduceBy:CGFloat = -4.0
        var output = [NSMutableAttributedString]()
        
        for ( _ , attribString) in attributedStrings.enumerated() {
            
            let string = attribString.string
            let newAttString = NSMutableAttributedString(string: string)
            
            for k in 0..<attribString.length {
                
                let range = NSRange(location: 0, length: attribString.length)
                var attribs = attribString.attributes(at: k, longestEffectiveRange: nil, in: range)

                if let offset = attribs[.baselineOffset] as? CGFloat {
                    let newOffset = offset + reduceBy
                    attribs.removeValue(forKey: .baselineOffset)
                    attribs[NSAttributedStringKey.baselineOffset] = newOffset
                }
                
                newAttString.addAttributes(attribs, range: range)
            }
            
            output.append(newAttString)
        }
        
        return output
    }
    
    // 81^^4 = 3, 81^^log(1000) = 3, (10 + 71)^^log(1000), log(1000)^^3, 81^^4^^2
    // (729^.5)^(1/3) = 3
    
    // "⬚" DOTTED SQUARE
    
    //  -5
    // ± - ⁻5 −
    // 5+-5
    // 5+−5
    // 5+⁻5
    
    private func textAttribute(font:UIFont, textColor:UIColor, level:Int, subLevel:Bool = false, kern:Bool = false) -> [NSAttributedStringKey: Any] {

        //var resizedPtSize = font.pointSize * CGFloat( powf(0.7, level) )
        
        var kernValue:CGFloat = 0
        if kern {
            kernValue = 10
        }
        
        var offset:CGFloat = 0.0
        var size:CGFloat = font.pointSize
        
        for _ in (0..<level) {
            kernValue *= 0.7
            size *= 0.7
            offset += size * 0.57
        }
        
        if subLevel {
            offset -= size * 0.3
            size *= 0.6
        }
        
        let resizedFont =  font.withSize(size)
        let baselineOffset = NSNumber(value: Float(offset))
        
        let attribute:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: textColor,
            NSAttributedStringKey.font: resizedFont,
            NSAttributedStringKey.baselineOffset: baselineOffset,
            NSAttributedStringKey.kern: CGFloat(kernValue)
        ]
        
        return attribute
    }
    
    private func increaseLineSpacing(attributedString:NSMutableAttributedString, lineHeightMultiple:CGFloat) -> NSMutableAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.lineSpacing = 0
        paragraphStyle.lineHeightMultiple = lineHeightMultiple // 1.2 looks good
        paragraphStyle.minimumLineHeight = 0
        paragraphStyle.alignment = .right
        
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
    
    private func addSpaceBetween(charSet:CharacterSet, arrayOfAttributedStrings: [NSMutableAttributedString]) -> [NSMutableAttributedString] {
        
        var theArray = arrayOfAttributedStrings
        
        for (i, theAttribString) in theArray.enumerated() {
            let token = theAttribString.string
            if token.count != 1 {
                continue
            }
            else if token.rangeOfCharacter(from: charSet) != nil {
    
                let mdasRange = NSRange(location:0, length:1)
                let mdasIndex = theArray.index(i, offsetBy: 0)
                let mdasAttribString = theArray[mdasIndex]
            
                var kernValue = CGFloat(10)
                if let font = fontFor(attributedString: mdasAttribString) {
                    kernValue = kernAmount(font: font)
                }
                
                mdasAttribString.addAttribute(NSAttributedStringKey.kern, value: kernValue, range: mdasRange)
                theArray[mdasIndex] = mdasAttribString
                
                if i > 0 {
                    let beforeIndex = theArray.index(i, offsetBy: -1)
                    let beforeAttribString = theArray[beforeIndex]
                    let loc = beforeAttribString.string.count - 1
                    let lastRange = NSRange(location:loc, length:1)
                    beforeAttribString.addAttribute(NSAttributedStringKey.kern, value: kernValue, range: lastRange)
                    theArray[beforeIndex] = beforeAttribString
                }
            }
        }
        
        return theArray
    }
    
    private func kernAmount(level:Int, font:UIFont) -> CGFloat {
        var kernValue:CGFloat = 10.0
        var size:CGFloat = font.pointSize
        
        for _ in (0..<level) {
            kernValue *= 0.7
            size *= 0.7
        }
        
        return kernValue
    }
    
    private func kernAmount(font:UIFont) -> CGFloat {
        let kernValue = font.pointSize * 0.18
        return kernValue
    }
    
    private func fontFor(attributedString:NSMutableAttributedString) -> UIFont? {
        let fontAttrib = attributedString.attribute(NSAttributedStringKey.font, at: 0, effectiveRange: nil)
        let font = fontAttrib as? UIFont
        return font
    }
    
    func testReorder() {
        
        let testEq = "( 9 nthRoot 2" // "81 nthRoot"
        
        let testInput = testEq.components(separatedBy: " ")
        
        let testOutput = reorderNthRoot(input: testInput)
        print(testOutput)
        
        var eq = "5 + ( 1 + 81 nthRoot 4 ) nthRoot 4 + 2"
        
        var input = eq.components(separatedBy: " ")
    
        var output = reorderNthRoot(input: input)
        
        eq = "6 + 81 nthRoot 4 nthRoot 2 + 5"
        
        input = eq.components(separatedBy: " ")
        
        output = reorderNthRoot(input: input)
        
        eq = "7 + ( 1 + 81 nthRoot 4 ) nthRoot ( 3 + 81 nthRoot 4 nthRoot 2 + 8 ) + 9"
        
        input = eq.components(separatedBy: " ")
        
        output = reorderNthRoot(input: input)
        
        eq = "3 ! nthRoot 2 !"
        
        input = eq.components(separatedBy: " ")
        
        output = reorderNthRoot(input: input)
        
        eq = "log ( 10000 ) nthRoot ( 3 + log ( 1000 ) )"
        
        input = eq.components(separatedBy: " ")
        
        output = reorderNthRoot(input: input)
        
        print("\(output)")
    }
    
    func removeCaret(input:[NSMutableAttributedString], placeHolder:String) -> [NSMutableAttributedString] {
        var output = [NSMutableAttributedString]()
        
        for ( _ , attribString) in input.enumerated() {
            let token = attribString.string
            if token != "^" {
                output.append(attribString)
                continue
            }
            
            // if placehoder attribute...
            if let isPlaceHolder = attribString.attribute(NSAttributedStringKey(rawValue: "PlaceHolder"), at: 0, effectiveRange: nil) as? Bool {
                if isPlaceHolder {
                    // add placeHolder
                    let caretAttribs = attribString.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: attribString.length))
                    let holderAttribText = NSMutableAttributedString(string:placeHolder, attributes: caretAttribs)
                    
                    output.append(holderAttribText)
                }
            }
        }
        
        return output
    }
    
    func reorderNthRoot(input:[NSMutableAttributedString], placeHolder:NSMutableAttributedString) -> [NSMutableAttributedString] {
        
        var output = [NSMutableAttributedString]()
        var skipOver = -1
        for (index, attribString) in input.enumerated() {
            
            let token = attribString.string
            
            if index <= skipOver {
                continue
            }
            else if token != "nthRoot" {
                output.append(attribString)
                continue
            }
            
            // find root part
            var rootPart = [NSMutableAttributedString]()
            var braceCount = 0
            for ( _ , opAttribString) in output.enumerated().reversed() {
                
                let op = opAttribString.string
                
                if op == "(" {
                    braceCount += 1
                }
                else if op == ")" {
                    braceCount -= 1
                }
                
                 let isLess = isPrecedenceLessThanOrEqual(op: op, refOp: "nthRoot")
                
                if (braceCount == 0 && isLess) || braceCount > 0 {
                    break
                }
                
                rootPart.insert(output.removeLast(), at: 0)
            }
            
            // find nth part
            var nthPart = [NSMutableAttributedString]()
            var inputAfterNthRoot = input.suffix(input.count-index-1)
            braceCount = 0
            for ( i , opAttribString) in inputAfterNthRoot.enumerated() {
                let op = opAttribString.string
                if op == "(" {
                    braceCount += 1
                }
                else if op == ")" {
                    braceCount -= 1
                }
                
                let isLess = isPrecedenceLessThan(op: op, refOp: "nthRoot")
                
                if (braceCount == 0 && isLess) || braceCount < 0 {
                    skipOver = i + index
                    break
                }
                skipOver = i + index + 1
                
                nthPart.append(inputAfterNthRoot.removeFirst() )
            }
            
            nthPart = reorderNthRoot(input: nthPart, placeHolder:placeHolder)
            
            let rootAttribs = attribString.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: attribString.length))
            let rootAttribText = NSMutableAttributedString(string:"√", attributes: rootAttribs)
            
            // add placeholder if nothing
            if nthPart.count == 0 {
                nthPart.append(placeHolder)
            }
            
            // add everything to output
            output.append(contentsOf: nthPart)
            output.append(rootAttribText)
            output.append(contentsOf: rootPart)
        }
        
        return output
    }
    

    private func reorderNthRoot(input:[String], dotBoxString:String = "⬚") -> [String] {
 
        var output = [String]()
        var skipOver = -1
        for (index, token) in input.enumerated() {
            
            if index <= skipOver {
                continue
            }
            else if token != "nthRoot" {
                output.append(token)
                continue
            }
            
            // find root part
            var rootPart = [String]()
            var braceCount = 0
            for ( _ , op) in output.enumerated().reversed() {
                
                if op == "(" {
                    braceCount += 1
                }
                else if op == ")" {
                    braceCount -= 1
                }
                
                let isLess = isPrecedenceLessThanOrEqual(op: op, refOp: "nthRoot")
                
                if (braceCount == 0 && isLess) || braceCount > 0 {
                    break
                }
            
                rootPart.insert(output.removeLast(), at: 0)
            }
            
            // find nth part
            var nthPart = [String]()
            var inputAfterNthRoot = input.suffix(input.count-index-1)
            braceCount = 0
            for ( i , op) in inputAfterNthRoot.enumerated() {
               
                if op == "(" {
                    braceCount += 1
                }
                else if op == ")" {
                    braceCount -= 1
                }
                
                let isLess = isPrecedenceLessThan(op: op, refOp: "nthRoot")
                
                if (braceCount == 0 && isLess) || braceCount < 0 {
                    skipOver = i + index
                    break
                }
                skipOver = i + index + 1
          
                nthPart.append(inputAfterNthRoot.removeFirst() )
            }
            
            nthPart = reorderNthRoot(input: nthPart)
            
            // add placeholder
            if nthPart.count == 0 {
                nthPart.append(dotBoxString)
            }

            // add everything to output
            output.append(contentsOf: nthPart)
            output.append("√")
            output.append(contentsOf: rootPart)
        }
        
        return output
    }
    
    private func isPrecedenceLessThanOrEqual(op:String, refOp:String) -> Bool {
        
        if let oper = operators[op], let ref = operators[refOp] {
            if oper.precedence <= ref.precedence {
                return true
            }
        }
        return false
    }
    
    private func isPrecedenceLessThan(op:String, refOp:String) -> Bool {
        
        if let oper = operators[op], let ref = operators[refOp] {
            if oper.precedence < ref.precedence {
                return true
            }
        }
        return false
    }

    func lastOpFormatted() -> String {
        
        let last = equation.last ?? ""
        
        if last.isNumber {
            
            return last.doubleFormatted(maxTotalDigits: maxDigits, maxFractionDigits: maxFractionDigits)!
            //return (Double(last)?.calculatorStyle2)!
        }
        
        // operation (ex: *,+ ,-,...)
        
        return last
    }
    
    func infixToPostfix(infix:[String], runningTotal:Bool = false) -> [String] {
        
        var input = infix
        var output = [String]()
        var stack = Stack<String>()
        
        repeat {
            let token:String = input.removeFirst()
            
            // if the token is a number, then push it to the output queue.
            
            if token.isNumber(alsoNumbers: Constants.allValues) {
                output.append(token)
            }
                
            // if the token is an operator, then:
                
            else if let tokenGroup = operators[token] {
                
                // while there is an operator at the top of the operator stack with
                // greater precedence:
                // pop operators from the operator stack, onto the output queue;
                
                var operatorStackGreaterThenToken = false
                
                repeat {
                    
                    operatorStackGreaterThenToken = false
                    
                    if let top = stack.top, let topGroup = operators[top] {
                        
                        if topGroup.precedence == tokenGroup.precedence {
                            
                            if topGroup.associativity.rawValue > tokenGroup.associativity.rawValue {
                                operatorStackGreaterThenToken = false
                            }
                            else if topGroup.associativity == .left && tokenGroup.associativity == .left {
                                operatorStackGreaterThenToken = true
                            }
                            else if topGroup.associativity == .right && tokenGroup.associativity == .right {
                                operatorStackGreaterThenToken = false
                            }
                        }
                        else if topGroup.precedence > tokenGroup.precedence {
                            operatorStackGreaterThenToken = true
                        }
                    }
                    
                    if operatorStackGreaterThenToken {
                        output.append(stack.pop())
                    }
                    
                    
                } while operatorStackGreaterThenToken
                
                // push the read operator onto the operator stack.
                let op = operators[token]!
                if input.isEmpty && runningTotal && !op.unary  {
                    print("test. not done")
                    
                }
                else {
                
                    stack.push(token)
                }
            }
                
            // if the token is a left bracket (i.e. "("), then:
                
            else if token == "(" {
                
                // push it onto the operator stack.
                
                stack.push(token)
            }
                
            // if the token is a right bracket (i.e. ")"), then:
                
            else if token == ")" {
                
                // while the operator at the top of the operator stack is not a left bracket:
                
                while stack.top != nil && stack.top! != "(" {
                    // pop operators from the operator stack onto the output queue.
                    
                    output.append(stack.pop())
                }
                
                // pop the left bracket from the stack.
                
                if stack.top != nil && stack.pop() != "(" {
                    print("error. operator is not )")
                }
                
                /*
                 *  if the stack runs out without finding a left bracket, then there are
                 *  mismatched parentheses.
                 *
                 */
            }
            
        } while input.count > 0
        
        // if equation is not fully entered in
        // yet, user want running total
        
        if runningTotal {
            
            let adjustedOperators = adjustPercentOp(postfix: stack)
            for op in adjustedOperators {
                if let opType = operators[op] {
                    if opType.unary && opType.associativity == .right {
                        output.append(op)
                        continue
                    }
                    break
                }
            }
            adjustPercentOp(postfix: &output)
            
            return output
        }
        
        // if there are no more tokens to read:
        
        // while there are still operator tokens on the stack:
        while stack.count > 0 {
            
            // if the operator token on the top of the stack is a bracket, then
            // there are mismatched parentheses.
            
            // pop the operator onto the output queue.
            output.append(stack.pop())
        }
        
        // convert '%' to '+or-%' if necessary
        
        adjustPercentOp(postfix: &output)
        
        return output
    }
    
    func adjustPercentOp(postfix: Stack<String>) -> [String] {
        var stack = postfix
        var output = [String]()
        while stack.count > 0 {
            output.append(stack.pop())
        }
        adjustPercentOp(postfix: &output)
        return output
    }
    
    func adjustPercentOp(postfix:inout [String]) {
        
        // input: "1000,40,%,%,%,-"
        // ouput: "1000,40,+or-%,+or-%,+or-%,-"
        
        let searchSequences = [ ["%", "-"], ["%", "+"] ]
        var index = postfix.count - searchSequences[0].count
        
        while index >= 0 {
            
            let subArray = postfix[index..<index + searchSequences[0].count]
            var sequenceMatched = false
            
            for sequence in searchSequences {
                
                if subArray.elementsEqual(sequence) {
                    sequenceMatched = true
                    break
                }
            }
            
            if sequenceMatched {
                postfix[index] = "+or-%" // replace '%' with '+or-%'
                index -= 1
                
                // now replace consecutive '%' with '+or-%'
                
                while index >= 0 {
                    if postfix[index] == "%" {
                        postfix[index] = "+or-%"
                        index -= 1
                    }
                    else {
                        break
                    }
                }
                continue
            }
            index -= 1
        }
        //print("adjusted postfix is \(postfix)")
    }
    
    func evaluate(postfix:[String]) throws -> Double? {
        var a:Double, b:Double
        var input = postfix
        var numbers = Stack<Double>()
        
        if input.count == 0 {
            return 0
        }
        
        repeat {
            
            let token = input.removeFirst()
            
            if token == "+" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push(a + b)
            }
            else if token == "-" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push(a - b)
            }
            else if token == "✕" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push(a * b)
            }
            else if token == "/" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push(a / b)
            }
            else if token == "^" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push( pow(a, b)  )
            }
            else if token == "max" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push(max(a,b))
            }
            else if token == "sin" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                switch angleType {
                case .degrees:
                    numbers.push(__sinpi(a/180.0))
                case .radians:
                    numbers.push(sin(a))
                }
            }
            else if token == "cos" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                switch angleType {
                case .degrees:
                    numbers.push(__cospi(a/180.0))
                case .radians:
                    numbers.push(cos(a))
                }
                // numbers.push(cos(a).roundTo(places: 5))
            }
            else if token == "tan" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                switch angleType {
                case .degrees:
                    numbers.push(__tanpi(a/180.0))
                case .radians:
                    numbers.push(tan(a))
                }
                // numbers.push(tan(a).roundTo(places: 5))
            }
            else if token == "cosh" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(cosh(a))
            }
            else if token == "sinh" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(sinh(a))
            }
            else if token == "tanh" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(tanh(a))
            }
            else if token == "asin" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                switch angleType {
                case .degrees:
                    a = asin(a) * 180.0 / Double.pi
                case .radians:
                    a = asin(a)
                }
                numbers.push(a)
            }
            else if token == "acos" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                switch angleType {
                case .degrees:
                    a =  acos(a) * 180.0 / Double.pi
                case .radians:
                    a = acos(a)
                }
                numbers.push(a)
            }
            else if token == "atan" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                switch angleType {
                case .degrees:
                    a = atan(a) * 180.0 / Double.pi
                case .radians:
                    a = atan(a)
                }
                numbers.push(a)
            }
            else if token == "acosh" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(acosh(a))
            }
            else if token == "asinh" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(asinh(a))
            }
            else if token == "atanh" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(atanh(a))
            }
            else if token == "nthRoot" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                b = numbers.pop()
                a = numbers.pop()
                numbers.push( pow(a, (1/b) )  )
            }

            else if token == "√" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(sqrt(a))
            }
            else if token == "∛" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(cbrt(a))
            }
            else if token == "log2" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(log2(a))
            }
            else if token == "log" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(log10(a))
            }
            else if token == "ln" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push(log(a))
            }
            else if token == "!" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                var fact: Double = 1
                let n: Int = Int(a) + 1
                for i in 1..<n {
                    fact = fact * Double(i)
                }
                numbers.push(fact)
            }
            else if token == "E" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                if numbers.count == 1{
                    a = numbers.pop()
                    numbers.push(a)
                }
                else if numbers.count >= 2 {
                    b = numbers.pop()
                    a = numbers.pop()
                    numbers.push(a * pow(10, b))
                }
            }
            else if token == "%" {
                if numbers.count < 1 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                numbers.push( a/100 )
            }
            else if token == "+or-%" {
                if numbers.count < 2 { throw ReversePolishNotation.operationMissingNumber }
                a = numbers.pop()
                b = numbers.peek()!
                numbers.push( b * a/100 )
            }

            else if let value = Double(token) {
                numbers.push(value)
            }
            else if Constants.allValues.contains(token) {
                if let value = Constants(rawValue: token){
                    numbers.push(value.double!)
                }
                else {
                    /* error */
                    print("unrecognized token constant \(token)")
                }
            }
            else {
                /*
                 *   error
                 */
                print("unrecognized token \(token)")
            }
            
        } while input.count > 0
        
        if numbers.isEmpty {
            return nil
        }
        
        return numbers.pop()
    }
    
}
