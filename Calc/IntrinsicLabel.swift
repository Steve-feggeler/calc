//
//  IntrinsicLabel.swift
//  Squares
//
//  Created by stephen feggeler on 11/9/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

class IntrinsicLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func layoutSubviews() {
        invalidateIntrinsicContentSize()
        super.layoutSubviews()
    }
    
    override func invalidateIntrinsicContentSize() {
        super.invalidateIntrinsicContentSize()
    }
    
    override var text: String? {
        didSet {
            adjustIntrinsicContentSize = true
        }
    }
    
    override var attributedText: NSAttributedString? {
        didSet {
            adjustIntrinsicContentSize = false
        }
    }
    
    private var adjustIntrinsicContentSize = true
    
    override var intrinsicContentSize: CGSize {
        get {
            if adjustsFontSizeToFitWidth /* adjustIntrinsicContentSize*/ {
                var currentFont: UIFont = font
                let originalFontSize = currentFont.pointSize
                var currentSize: CGSize = (text! as NSString).size(withAttributes: [NSAttributedStringKey.font: currentFont])
                
                while currentSize.width > frame.size.width && currentFont.pointSize > (originalFontSize * minimumScaleFactor) {
                    currentFont = currentFont.withSize(currentFont.pointSize - 1)
                    currentSize = (text! as NSString).size(withAttributes: [NSAttributedStringKey.font: currentFont])
                }
                
                currentSize.width = currentSize.width.round(nearest: 0.5)
                currentSize.height = currentSize.height.round(nearest: 0.5)
                
                /*
                let l1 = UILabel()
                l1.adjustsFontSizeToFitWidth = true
                l1.minimumScaleFactor = 0.5
                l1.font = self.font.withSize(60)
                l1.attributedText = self.attributedText
                */
                
                return currentSize
            }
            else if adjustIntrinsicContentSize == false {
                
                guard let theSize = attributedText?.size() else {
                    return super.intrinsicContentSize
                }
                
                return theSize
            }
            
            return super.intrinsicContentSize
        }
    }

}
