//
//  OperationSymbol.swift
//  Calc
//
//  Created by stephen feggeler on 1/19/18.
//  Copyright © 2018 stephen feggeler. All rights reserved.
//

import UIKit

class OperationSymbol {
    var color:UIColor?
    var symbols:[String]
    var name:String
    
    init(name:String, symbols:[String]) {
        self.symbols = symbols
        self.name = name
    }
    
    func testCode() {
        let token = symbols.first!
        
        if token == OperationSymbol.add {
            
        }
        else if token == OperationSymbol.multiply {
            
        }
        else if OperationSymbol.multiply.symbols.contains(token) {
            
        }
        else if OperationSymbol.multiply.matches(token) {
            
        }
        
        let elements = ["1"]
        if elements.contains("1") {
            
        }
    }
    
    func matches(_ symbol:String) -> Bool {
        return symbols.contains(symbol)
    }
    
    static func == (op:OperationSymbol, symbol:String) -> Bool {
        return op.symbols.contains(where: { $0 == symbol })
    }
    
    static func == (symbol:String, op:OperationSymbol ) -> Bool {
        return op.symbols.contains(where: { $0 == symbol })
    }
    
    static let max = OperationSymbol(name: "max", symbols: ["max"])
    static let sin = OperationSymbol(name: "sin", symbols: ["sin"])
    static let cos = OperationSymbol(name: "cos", symbols: ["cos"])
    static let tan = OperationSymbol(name: "tan", symbols: ["tan"])
    static let sinh = OperationSymbol(name: "sinh", symbols: ["sinh"])
    static let cosh = OperationSymbol(name: "cosh", symbols: ["cosh"])
    static let tanh = OperationSymbol(name: "tanh", symbols: ["tanh"])
    static let asin = OperationSymbol(name: "asin", symbols: ["asin"])
    static let acos = OperationSymbol(name: "acos", symbols: ["acos"])
    static let atan = OperationSymbol(name: "atan", symbols: ["atan"])
    static let asinh = OperationSymbol(name: "asinh", symbols: ["asinh"])
    static let acosh = OperationSymbol(name: "acosh", symbols: ["acosh"])
    static let atanh = OperationSymbol(name: "atanh", symbols: ["atanh"])
    static let log = OperationSymbol(name: "log", symbols: ["log"])
    static let log2 = OperationSymbol(name: "log2", symbols: ["log2"])
    static let ln = OperationSymbol(name: "ln", symbols: ["ln"])
    static let sqrt = OperationSymbol(name: "sqrt", symbols: ["√"])
    static let cubeRoot = OperationSymbol(name: "cubeRoot", symbols: ["∛"])
    static let nthRoot = OperationSymbol(name: "nthRoot", symbols: ["nthRoot"])
    static let power = OperationSymbol(name: "power", symbols: ["^"])
    static let exponent = OperationSymbol(name: "exponent", symbols: ["E"])
    static let factorial = OperationSymbol(name: "factorial", symbols: ["!"])
    static let percent = OperationSymbol(name: "percent", symbols: ["%"])
    static let specialPercent = OperationSymbol(name: "specialPercent", symbols: ["+or-%"])
    static let add = OperationSymbol(name: "add", symbols: ["+"])
    static let subtract = OperationSymbol(name: "subtract", symbols: ["-"])
    static let multiply = OperationSymbol(name: "multiply", symbols: ["*","✕","x","X"])
    static let divide = OperationSymbol(name: "divide", symbols: ["÷","/"])
    
}
