//
//  ViewController.swift
//  Calc
//
//  Created by stephen feggeler on 5/25/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var mathEquation = MathEquation()
    var prevButtonPressed:UIButton?
    var angleType = MathEquation.AngleType.degrees
    
    /// Some buttons have two states.
    /// Two state type for button.
    enum ButtonState {
         case first, second
         mutating func next() {
             switch self {
                case .first: self = .second
                case .second: self = .first
            }
        }
    }
    /// current state for second function button
    var buttonState:ButtonState = .first
    
    /// equation view set with attributed string
    var equationAttributedString:NSAttributedString? {
        set { equationView.attributedText = newValue }
        get { return equationView.attributedText }
    }
    
    /// equation view set with string
    var equationText:String? {
        set { equationView.text = newValue }
        get { return equationView.text }
    }
    
    /// running totoal view set with string
    var runningTotalText:String? {
        set {
            if mathEquation.termCount > 1 {
               runningTotalLabel.text = newValue
            }
            else {
               runningTotalLabel.text? = ""
            }
        }
        get {
            return runningTotalLabel.text
        }
    }
    
    /// outlets to subview controls in storyboard
    @IBOutlet weak var equationView: LabelScrollable!
    @IBOutlet weak var equationScrollView: UIScrollView!
    @IBOutlet weak var runningTotalLabel: UILabel!
    @IBOutlet weak var clearButton: ColorButton!
    @IBOutlet weak var equationLabelWidth: NSLayoutConstraint!
    
    @IBOutlet weak var secondFunctionButton: ColorButton!
    @IBOutlet weak var x2Button: ColorButton!
    @IBOutlet weak var x3Button: ColorButton!
    @IBOutlet weak var xPowerY: ColorButton!
    @IBOutlet weak var ePowerX: ColorButton!
    @IBOutlet weak var tenPowerX: ColorButton!
    @IBOutlet weak var log10Button: ColorButton!
    @IBOutlet weak var xsqrtyButton: ColorButton!
    
    @IBOutlet weak var sinAndInverseButton: ColorButton!
    @IBOutlet weak var cosAndInverseButton: ColorButton!
    @IBOutlet weak var tanAndInvserseButton: ColorButton!
    @IBOutlet weak var sinhAndInverseButton: ColorButton!
    @IBOutlet weak var coshAndInverseButton: ColorButton!
    @IBOutlet weak var tanhAndInverseButton: ColorButton!
    @IBOutlet weak var angleUnitButton: ColorButton!
    
    /// number of digits in calculator depends on view width
    private func updateEquationFormat(viewSize:CGSize) {
        if viewSize.width < 440 {
            mathEquation.maxDigits = 12
            mathEquation.maxFractionDigits = 11
        }
        else {
            mathEquation.maxDigits = 16
            mathEquation.maxFractionDigits = 15
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.updateDisplay()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print("traitCollectionDidChange size: \(self.view.bounds.size)")
        updateEquationFormat(viewSize: view.bounds.size)
        self.formatButtons()
        self.updateDisplay()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition:
            
            { _ in
                
                // animate along side here
                
            },
                    completion:
            { _ in
                
                // completion code here
              
            }
        )
    }
    
    func updateDisplay() {
        let fontDesign = equationView.designFontAndMinimumScaleFactor()
        let eqAttributedString =
            mathEquation.joinedAndFormatted(
                font: fontDesign.font,
                textColor: equationView.labelView.textColor,
                minimumScaleFactor: fontDesign.minimumScaleFactor,
                minContainerSize: equationView.scrollView.bounds.size)
        
        equationAttributedString = eqAttributedString
        runningTotalText = mathEquation.runningTotal()
    }
    
    func buttonPressed(_ sender: UIButton) {
        // remove lines
        if let button = prevButtonPressed as? ColorButton {
            button.isPending = false
        }
        
        prevButtonPressed = sender        
    }
    @IBAction func buttonDown(_ sender: ColorButton) {
        
        // create a sound ID, in this case its the tweet sound.
        let systemSoundID: SystemSoundID = 1104 // tock.caf
        
        // to play sound
        AudioServicesPlaySystemSound (systemSoundID)
    }
    
    @IBAction func signPressed(_ sender: UIButton) {
        buttonPressed(sender)
        mathEquation.sign()
        updateDisplay()
    }
    
    @IBAction func percentPressed(_ sender: UIButton) {
        buttonPressed(sender)
        mathEquation.percent()
        updateDisplay()
    }
    
    @IBAction func decimalPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(decimal: ".")        
        updateDisplay()
    }
    
    @IBAction func operationPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        guard let operation = sender.titleLabel?.text else { return }
        
        let op:String
        
        switch operation {
            case "+":
                print("plus pressed")
                op = "+"
                
            case "-":
                print("subtract pressed")
                op = "-"
                
            case "✕":
                print("multiply pressed")
                op = "✕"
            
            case "÷":
                print("divide pressed")
                op = "/"
                
            default:
                print("unknown operation")
                op = operation
        }
        
        if mathEquation.canAppendOperation() == false {
            return
        }
        
        if let button = sender as? ColorButton {
            button.isPending = true
        }
        
        mathEquation.append(operation: op)
        updateDisplay()
    }
    
    @IBAction func numberPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        guard let digit = sender.titleLabel?.text else { return }
        mathEquation.append(digit: digit)
        clearButton.titleLabel?.text = "C"
        updateDisplay()
    }
    
    @IBAction func equalPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        let termCount = mathEquation.termCount
        let answer:String? = mathEquation.equal()
        
        print("answer is \(String(describing: answer)))")
        
        if termCount <= 1 {
            
            // don't animate answer
            
            equationText = mathEquation.joinedAndFormatted()
            runningTotalLabel.text = ""
        }
        else {
            
            // animate answer from runningTotal to equation...
            
            if let answer = answer {
                
                runningTotalLabel.text = answer
                equationView.animateText(withDuration: 0.35, fromLabel: runningTotalLabel, completion: { (success) in
                    self.updateDisplay()
                })
                
            }
            else {
                let message = "Error"
                runningTotalLabel.text = message
                
                equationView.animateText(withDuration: 0.35, fromLabel: runningTotalLabel, completion: { (success) in
                    self.equationText = message
                    self.runningTotalText = ""
                })
            }
        }
        
        /*
        let eq = [
            // 3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3
            ["3","+","4","*","2","/","(", "1", "-","5",")","^","2","^","3"],
            ["5","^","4"],
            ["2","+","(","1","-","5",")"],
            ["(","1","-","5",")"],
            // sin ( max ( 2, 3 ) / 3 * 3.1415 )
            ["sin","(","max","(","2","3",")","/","3","*","3.1415926",")"],
            ["sin", "3.1415926"]
        ]
        */
    }
    
    @IBAction func clearPressed(_ sender: UIButton) {
        //print("touchUp inside clear fired !!!")
        buttonPressed(sender)
        mathEquation.removeLast()
        clearButton.titleLabel?.text = "AC"
        updateDisplay()
    }
    
    @IBAction func clearLongPressed(_ sender: UILongPressGestureRecognizer) {
     
        if sender.state == .ended {
           print("clearLongPressed for AC")
            buttonPressed(sender.view as! UIButton)
            mathEquation.clear()
            clearButton.titleLabel?.text = "AC"
            updateDisplay()
        }
     }
    
    @IBAction func rightRoundBracket(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.appendRightRoundBracket()
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func leftRoundBracket(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.appendLeftRoundBracket()
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func tenToPower(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.tenToPower()
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func eToPowerX(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(constant: .e)
        mathEquation.append(operation:"^")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func xToPowerY(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        if mathEquation.canAppendOperation() == false {
            return
        }
        
        mathEquation.append(operation:"^")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func xToPower3(_ sender: UIButton) {
        buttonPressed(sender)
        
        if mathEquation.canAppendOperation() == false {
            return
        }
        
        mathEquation.append(operation:"^")
        mathEquation.append(digit:"3")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func xToPower2(_ sender: UIButton) {
        buttonPressed(sender)
        
        if mathEquation.canAppendOperation() == false {
            return
        }
        
        mathEquation.append(operation:"^")
        mathEquation.append(digit:"2")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func pi(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(constant: MathEquation.Constants.π)
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func exp(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(constant: MathEquation.Constants.e)
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func RandomNumber(_ sender: UIButton) {
        
        buttonPressed(sender)
        let betweenZeroAndOne = drand48()
        mathEquation.append(double:betweenZeroAndOne)
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func sinPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
            case .first: mathEquation.append(function: "sin")
            case .second: mathEquation.append(function: "asin")
        }
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func cosPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
            case .first: mathEquation.append(function: "cos")
            case .second: mathEquation.append(function: "acos")
        }
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func tanPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
        case .first: mathEquation.append(function: "tan")
        case .second: mathEquation.append(function: "atan")
        }
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func sinhPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
            
        case .first: mathEquation.append(function: "sinh")
        case .second: mathEquation.append(function: "asinh")
            
        }
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func coshPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
            
        case .first: mathEquation.append(function: "cosh")
        case .second: mathEquation.append(function: "acosh")
            
        }
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func tanhPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
            
        case .first: mathEquation.append(function: "tanh")
        case .second: mathEquation.append(function: "atanh")
            
        }

        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func sqrtPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(function: "√")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func cubeRootPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(function: "∛")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func nthrootPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(operation:"nthRoot")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    
    @IBAction func log10Pressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        switch buttonState {
            case .first: mathEquation.append(function: "log")
            case .second: mathEquation.append(function: "log2")
        }
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func lnPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.append(function: "ln")
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func factorialPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        mathEquation.factorial()
        updateDisplay()
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func inversePressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        if mathEquation.canAppendOperation() == false {
            return
        }
        
        mathEquation.append(operation:"^")
        mathEquation.append(digit:"-1")
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func secondFunctionPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        buttonState.next()
        formatSecondFunctionButtons()
    }
    
    @IBAction func angleUnitPressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        angleType.next()
        mathEquation.angleType = angleType
        
        switch angleType {
        case .degrees:
            angleUnitButton.setTitle("Rad",for: .normal)
            
        case .radians:
            angleUnitButton.setTitle("Deg",for: .normal)
        }
    }
    
    @IBAction func eePressed(_ sender: UIButton) {
        
        buttonPressed(sender)
        if mathEquation.scientificNotation() {
            updateDisplay()
        }
    }
    
    func demoForCharacterSet()
    {
        let str = "abcdef12345"
        let digitSet = CharacterSet.decimalDigits
        
        for ch in str.unicodeScalars {
            if digitSet.contains(ch) {
                // is digit!
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        runningTotalLabel.text = ""
        
        equationView.labelView.firstBaselineAnchor.constraint(equalTo: runningTotalLabel.topAnchor, constant: -15).isActive = true
        
        //UIApplication.shared.statusBarStyle = .lightContent
        
        self.mathEquation.clear()
    
        clearButton.titleLabel?.textAlignment = .center
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController {
    
    func formatButtons() {
        
        formatPower(button: x2Button)
        formatPower(button: x3Button)
        formatPower(button: xPowerY)
        formatPower(button: ePowerX)
        formatText(text: "10", superscript: "x", button: tenPowerX)
        formatText(text: "2", superscript: "nd", button: secondFunctionButton)    
        
        formatSecondFunctionButtons()
    }
    
    func formatSecondFunctionButtons() {
        
        switch buttonState {
            
        case .first:
            formatText(text: "sin", superscript: "", button: sinAndInverseButton)
            formatText(text: "cos", superscript: "", button: cosAndInverseButton)
            formatText(text: "tan", superscript: "", button: tanAndInvserseButton)
            formatText(text: "sinh", superscript: "", button: sinhAndInverseButton)
            formatText(text: "cosh", superscript: "", button: coshAndInverseButton)
            formatText(text: "tanh", superscript: "", button: tanhAndInverseButton)
            formatText(text: "log", subscriptText: "10", button: log10Button)
            
        case .second:
            formatText(text: "sin", superscript: "-1", button: sinAndInverseButton)
            formatText(text: "cos", superscript: "-1", button: cosAndInverseButton)
            formatText(text: "tan", superscript: "-1", button: tanAndInvserseButton)
            formatText(text: "sinh", superscript: "-1", button: sinhAndInverseButton)
            formatText(text: "cosh", superscript: "-1", button: coshAndInverseButton)
            formatText(text: "tanh", superscript: "-1", button: tanhAndInverseButton)
            formatText(text: "log", subscriptText: "2", button: log10Button)
        }
    }
    
    func formatText(text:String, superscript:String, button:UIButton) {
    
        let font = button.titleLabel?.font
        
        let textAtrib:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: button.titleLabel!.textColor,
            NSAttributedStringKey.font: font!
        ]
        
        let superFont = button.titleLabel?.font.withSize((button.titleLabel?.font.pointSize)! * 0.5)
        
        let superAttrib:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: button.titleLabel!.textColor,
            NSAttributedStringKey.font: superFont!,
            NSAttributedStringKey.baselineOffset: 10.0
        ]
        
        let partOne = NSMutableAttributedString(string: text, attributes: textAtrib)
        let partTwo = NSMutableAttributedString(string: superscript, attributes: superAttrib)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        
        button.setAttributedTitle(combination, for: .normal)
    }
    
    func formatText(text:String, subscriptText:String, button:UIButton) {
        
        let font = button.titleLabel?.font.withSize((button.titleLabel?.font.pointSize)!)
        
        let textAtrib:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: button.titleLabel!.textColor,
            NSAttributedStringKey.font: font!
        ]
        
        let subFont = button.titleLabel?.font.withSize((button.titleLabel?.font.pointSize)! * 0.5)
        
        let subAttrib:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: button.titleLabel!.textColor,
            NSAttributedStringKey.font: subFont!,
            NSAttributedStringKey.baselineOffset: -7.0
        ]
        
        let partOne = NSMutableAttributedString(string: text, attributes: textAtrib)
        let partTwo = NSMutableAttributedString(string: subscriptText, attributes: subAttrib)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        
        button.setAttributedTitle(combination, for: .normal)
    }

    
    func formatPower(button:UIButton) {
        
        let firstChar = button.titleLabel?.text?.first!
        let lastChar = button.titleLabel?.text?.last!
        
        let text = String(firstChar!)
        let superscript = String(lastChar!)
        
        formatText(text: text, superscript: superscript, button: button)
    }
}

extension ViewController {
    
    @IBAction func memoryRead(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        let memoryDouble = MemoryValue.value()
        
        mathEquation.append(double:memoryDouble)
        
        updateDisplay()
        
        clearButton.titleLabel?.text = "C"
    }
    
    @IBAction func memorySubtract(_ sender: UIButton) {
        
        buttonPressed(sender)
        
        if let lastNumber = mathEquation.lastNumber() {
            MemoryValue.subtract(value: lastNumber)
        }
    }
    
    @IBAction func memoryAdd(_ sender: UIButton) {
        buttonPressed(sender)
        
        if let lastNumber = mathEquation.lastNumber() {
            MemoryValue.add(value: lastNumber)
        }
    }
    
    @IBAction func memoryClear(_ sender: UIButton) {
        buttonPressed(sender)
        MemoryValue.clear()
    }
}

