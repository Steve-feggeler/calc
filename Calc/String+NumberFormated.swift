//
//  String+Double.swift
//  Calc
//
//  Created by stephen feggeler on 8/14/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

extension String {
    func trimTrailingDotZero() -> String {
        if self.hasSuffix(".0") {
            return String(self.dropLast(2))
        }
        return self
    }
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    var digits2: String {
        let justNumbers = CharacterSet(charactersIn: "0123456789")
        return components(separatedBy: justNumbers.inverted).joined()
    }
    
    var isNumber : Bool { return self.doubleValue != nil }
    var doubleValue: Double? { return Double(self) }
    var floatValue: Float? { return Float(self) }
    var integerValue: Int? { return Int(self) }
    
    func isNumber(alsoNumbers:[String]) -> Bool {
        
        if alsoNumbers.contains(self) {
            return true
        }
        
        return self.isNumber
    }
    
    func doubleFormatted(maxTotalDigits:Int = 12, maxFractionDigits:Int = 12) -> String? {
        
        if !self.isNumber {
            return nil
        }
        
        var intDigits = 0
        var fractionDigits = 0
        var appendDecimal = false
        
        if self.contains("e"){
            let parts = self.components(separatedBy: "e")
            if parts.count > 1 {
                if let exponent = Int(parts[1]) {
                    if exponent > 0 {
                        intDigits = exponent + 1
                    }
                    else {
                        intDigits = 1
                        fractionDigits = abs(exponent)
                    }
                }
                
            }
        }
        else if self.contains(".") {
            let numberParts = self.components(separatedBy: ".")
            intDigits = numberParts[0].digits.count
            intDigits = max(1, intDigits)
            if numberParts.count > 1 {
                fractionDigits = numberParts[1].count
                // if we are "5." need to append "."
                if fractionDigits == 0 && intDigits < maxTotalDigits {
                    appendDecimal = true
                }
            }
        }
        else {
            intDigits = self.count
        }
        
        let maximumIntegerDigits = min(intDigits, maxTotalDigits)
        let maximumFractionDigits = min((maxTotalDigits - maximumIntegerDigits), maxTotalDigits)
        
        fractionDigits = min(fractionDigits, maximumFractionDigits)
        
        let formatter = NumberFormatter()
        formatter.locale = NSLocale.current
        formatter.numberStyle = .decimal
        formatter.usesGroupingSeparator = true
        formatter.minimumFractionDigits = fractionDigits
        formatter.maximumFractionDigits = maximumFractionDigits
        formatter.minimumIntegerDigits = 1
        formatter.maximumIntegerDigits = maximumIntegerDigits
        formatter.roundingMode = .down
        
        if appendDecimal {
            formatter.alwaysShowsDecimalSeparator = true
        }
        
        let ourValue = Double(self)!
        
        // if given 0.0000 just limit number of digits
        if ourValue == 0.0 {
            
            let totalDigits = intDigits + fractionDigits
            let dropCount = max(totalDigits - maxTotalDigits, 0)
            let formattedString = String(self.dropLast(dropCount))
            
            return formattedString
        }
        
        let maxDecimal:Double = pow(10.0, Double(maxTotalDigits)) - 1.0
        let minDecimal:Double = pow(10.0, -Double(maxTotalDigits-1))
        
        if ourValue > maxDecimal || ourValue < -maxDecimal || ((ourValue > 0.0) && (ourValue < minDecimal)) {
            
            var positiveFormat = "0."
            for _ in 1...(maxTotalDigits-4){
                positiveFormat += "#"
            }
            positiveFormat += "E0"
            
            formatter.numberStyle = .scientific
            formatter.positiveFormat = positiveFormat
            formatter.exponentSymbol = "e"
        }
        
        let formattedNumber = formatter.string(from: NSNumber(value:ourValue))
        
        return formattedNumber
    }

    
    func doubleValueFormatted(maxTotalDigits:Int = 12, maxFractionDigits:Int = 12) -> String? {
        
        var formattedDouble: String?
        
        if isNumber {
            
            let doubleValue = Double(self)!
            
            var minSizeString = "0."
            for _ in 1..<maxTotalDigits-1 {
                minSizeString += "0"
            }
            minSizeString += "1000001"
            let minSize:Double = Double(minSizeString)!
            //let minSize:Double = pow(10, -Double(maxTotalDigits))
            let isBetweenZeroAndMinSize = doubleValue > 0.0 && doubleValue < minSize
            let isBetweenZeroAndMinusMinSize = doubleValue < 0.0 && doubleValue > -1.0 * minSize
            let isVerySmallNumber = isBetweenZeroAndMinSize || isBetweenZeroAndMinusMinSize
            
            if self.contains("e") || isVerySmallNumber && false {
                return doubleValue.calculatorStyle(maxDigits: maxTotalDigits, maxFractionDigits: 0)
            }
            
            var integerPart:String?, fractionPart:String?
            var stringArray = Array<String>()
            var intDigitCount = 0
            var maxedOutDigits = false
            
            for (index, part) in self.components(separatedBy: ".").enumerated() {
                if index == 0, let intValue = part.doubleValue {
                    intDigitCount = part.count
                    if intDigitCount >= maxTotalDigits {
                        maxedOutDigits = true
                    }
                    integerPart = intValue.calculatorStyle(maxDigits: maxTotalDigits, maxFractionDigits: 0)
                }
                else if index == 1 && !maxedOutDigits {                    
                    fractionPart = part
                }
            }
            
            if let integerPart = integerPart {
                
                stringArray.append(integerPart)
            }
            
            if var fractionPart = fractionPart {
                
                stringArray.append(".")
                
                // limit fractionPart length
                
                var digitRemaining = maxTotalDigits - intDigitCount
                
                digitRemaining = min(digitRemaining, maxFractionDigits)
                
                let dropCount = fractionPart.count - digitRemaining
                
                if dropCount > 0 {
                    
                    fractionPart = String(fractionPart.dropLast(dropCount))
                }
                
                stringArray.append(fractionPart)
            }
            
            formattedDouble = stringArray.joined()
        }
        
        return formattedDouble
    }
}
