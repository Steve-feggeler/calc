//
//  Memory.swift
//  Calc
//
//  Created by stephen feggeler on 8/20/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import Foundation

struct MemoryValue {
    private static let key = "memoryValueKey"
    private static var _value:Double {
        get { return UserDefaults.standard.double(forKey: MemoryValue.key) }
        set { UserDefaults.standard.set(newValue, forKey: MemoryValue.key) }
    }
    static func clear() { _value = 0.0 }
    static func add(value:Double) { _value += value }
    static func subtract(value:Double) { _value -= value }
    static func value() -> Double { return _value }
}
