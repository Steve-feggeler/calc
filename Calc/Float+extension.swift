//
//  Float+extension.swift
//  Squares
//
//  Created by stephen feggeler on 11/9/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

extension CGFloat {    
    func round(nearest: CGFloat) -> CGFloat {
        let n = 1/nearest
        let numberToRound = self * n
        return numberToRound.rounded() / n
    }
}

extension Double {
    func round(nearest: Double) -> Double {
        let n = 1/nearest
        let numberToRound = self * n
        return numberToRound.rounded() / n
    }
    
    func floor(nearest: Double) -> Double {
        let intDiv = Double(Int(self / nearest))
        return intDiv * nearest
    }
}
