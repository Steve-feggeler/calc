//
//  UIScrollView+scrollTo.swift
//  Calc
//
//  Created by stephen feggeler on 8/14/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: true)
    }
    
    func scrollToRight() {
        let rightOffset = CGPoint(x: contentSize.width - bounds.size.width + contentInset.right, y: 0)
        setContentOffset(rightOffset, animated: false)
    }
}
