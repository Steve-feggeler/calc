//
//  LabelScrollable.swift
//  Squares
//
//  Created by stephen feggeler on 11/13/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

@IBDesignable class LabelScrollable: UIView {
    
    let labelView = UILabel()
    let scrollView = UIScrollView()
    let contentView = UIView()
    var contentWidthConstraint:NSLayoutConstraint!
    
    @IBInspectable var fontSize: CGFloat = 70
    @IBInspectable var fontSize_rr: CGFloat = 70    // fontSize_wh
    @IBInspectable var fontSize_rc: CGFloat = 70
    @IBInspectable var fontSize_cr: CGFloat = 60
    @IBInspectable var fontSize_cc: CGFloat = 40
    
    @IBInspectable var minScale: CGFloat = 1.0
    @IBInspectable var minScale_rr: CGFloat = 0.5   //minScale_wh
    @IBInspectable var minScale_rc: CGFloat = 0.5
    @IBInspectable var minScale_cr: CGFloat = 0.5
    @IBInspectable var minScale_cc: CGFloat = 1.0
    
    private var _setWithAttributedText = false
    
    @IBInspectable var text:String? = "Hello Abby! Bark! bark?" {
        didSet {
            _setWithAttributedText = false
            labelView.text = text
            updateLabelFontSize()
            resizeContent()
            //setNeedsLayout()
            self.superview?.setNeedsLayout()
        }
    }
    
    var attributedText:NSAttributedString? {
        didSet {
            print("LabelScroller.attributedText: \(String(describing: attributedText?.string)) size: \(String(describing: attributedText?.size()))")
        
            _setWithAttributedText = true
            labelView.attributedText = attributedText
            resizeContent()
            self.superview?.setNeedsLayout()
            self.superview?.layoutIfNeeded()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeProperties()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initializeProperties()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        resizeContent()
    }
    
    func estTextSize(text:String?) -> CGSize {
        
        let fontInfo = designFontAndMinimumScaleFactor()
        
        var currentFont: UIFont = fontInfo.font
        let originalFontSize = currentFont.pointSize
        var currentSize: CGSize = (text! as NSString).size(withAttributes: [NSAttributedStringKey.font: currentFont])
        
        while currentSize.width > bounds.size.width && currentFont.pointSize > (originalFontSize * fontInfo.minimumScaleFactor) {
            currentFont = currentFont.withSize(currentFont.pointSize - 1)
            currentSize = (text! as NSString).size(withAttributes: [NSAttributedStringKey.font: currentFont])
        }
        
        currentSize.width = currentSize.width.round(nearest: 0.5)
        currentSize.height = currentSize.height.round(nearest: 0.5)
        
        return currentSize
    }
    
    
    var contentSize:CGSize {
        get {
            let fontInfo = designFontAndMinimumScaleFactor()
            let smallestPtSize = fontInfo.font.pointSize * fontInfo.minimumScaleFactor

            let refLabel = UILabel()
            refLabel.adjustsFontSizeToFitWidth = true
            refLabel.font = fontInfo.font.withSize(smallestPtSize)
            refLabel.text = labelView.text
            let smallFontSize = refLabel.intrinsicContentSize
            
            refLabel.font = fontInfo.font
            refLabel.text = labelView.text
            let largeFontSize = refLabel.intrinsicContentSize
            
            if largeFontSize.width <= bounds.width {
                return largeFontSize
            }
            else if smallFontSize.width >= bounds.width {
                return smallFontSize
            }
            else {
                return estTextSize(text: labelView.text)
            }
        }
    }
    
    var baselineDelta:CGFloat {
        get {
            let fontInfo = designFontAndMinimumScaleFactor()
        
            let largeFontLabel = UILabel()
            largeFontLabel.adjustsFontSizeToFitWidth = true
            largeFontLabel.font = fontInfo.font
            largeFontLabel.text = labelView.text
            
            let estFontSize = fontInfo.font.pointSize * contentSize.width / largeFontLabel.intrinsicContentSize.width
            let fontSizeDiff = fontInfo.font.pointSize - estFontSize
            
            return 0.43 * fontSizeDiff - 0.066 * fontInfo.font.pointSize
        }
    }
    
    var baselineDelta2:CGFloat {
        get {
            let fontInfo = designFontAndMinimumScaleFactor()
            
            let largeFontLabel = UILabel()
            largeFontLabel.adjustsFontSizeToFitWidth = true
            largeFontLabel.font = fontInfo.font
            largeFontLabel.text = labelView.text
            
            let estFontSize = fontInfo.font.pointSize * contentSize.width / largeFontLabel.intrinsicContentSize.width
            let fontSizeDiff = fontInfo.font.pointSize - estFontSize
            
            return -0.43 * fontSizeDiff + 0.045 * fontInfo.font.pointSize
        }
    }
    
    
    private func fromTransform(fromLabel:UILabel) -> (transform:CGAffineTransform, dY:CGFloat) {
        
        var dX = fromLabel.frame.maxX - self.frame.maxX
        var dY = self.frame.maxY - fromLabel.frame.maxY
        
        dY += (fromLabel.bounds.height - fromLabel.intrinsicContentSize.height)/2
        //dY += 4
        dY += baselineDelta2
        print("baselineDelta \(baselineDelta)")

        //let toMaxWidth = min(labelView.bounds.size.width, labelView.intrinsicContentSize.width)
        let toMaxWidth = contentSize.width
        let ratio = fromLabel.intrinsicContentSize.width / toMaxWidth
        
        print("ratio = \(ratio)")
        
        dX =  -(self.bounds.size.width * ratio - self.bounds.size.width)/2.0 + dX
        dY =  (labelView.bounds.size.height * ratio - labelView.bounds.size.height)/2 + dY
        
        let trans = CGAffineTransform.identity.translatedBy(x: dX, y: -dY).scaledBy(x: ratio, y: ratio)
        
        let result = (trans, dY)
        
        return result
    }
    
    func animateText(withDuration duration:TimeInterval, fromLabel:UILabel, completion:((Bool) -> Swift.Void)? = nil) {
        let snapshot = scrollView.snapshotView(afterScreenUpdates: false)!
        snapshot.frame = scrollView.frame
        addSubview(snapshot)
        
      //  updateLabelFontSize()
        print("labelView pointSize: \(labelView.font.pointSize) scale: \(labelView.minimumScaleFactor)")
        self.text = fromLabel.text
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let fromTrans = fromTransform(fromLabel: fromLabel)
        labelView.transform = fromTrans.transform
        
        // need this to animate the text moving
        scrollView.clipsToBounds = false
        
        labelView.baselineAdjustment = .alignBaselines
        fromLabel.baselineAdjustment = .alignBaselines
      //  fromLabel.alpha = 0
        
        UIView.animate(withDuration: duration, animations:
        {
                snapshot.transform = CGAffineTransform(translationX: 0, y: fromTrans.dY*2)
                snapshot.alpha = 0.0
                
                self.labelView.transform = CGAffineTransform.identity
                self.layoutIfNeeded()
        },
                       completion:
        {
                (success) in
                
                snapshot.removeFromSuperview()
                fromLabel.alpha = 1
                
                // finished animating, clip back 'on'.
                self.scrollView.clipsToBounds = true
                
                if let completion = completion {
                    completion(success)
                }
        })
    }
    
    func designFontAndMinimumScaleFactor() -> (font:UIFont, minimumScaleFactor:CGFloat) {
        let designLabel = UILabel()
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular {
            designLabel.font = labelView.font.withSize(fontSize_rr)
            designLabel.minimumScaleFactor = minScale_rr
        }
        else if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .compact {
            designLabel.font = labelView.font.withSize(fontSize_rc)
            designLabel.minimumScaleFactor = minScale_rc
        }
        else if traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .regular {
            designLabel.font = labelView.font.withSize(fontSize_cr)
            designLabel.minimumScaleFactor = minScale_cr
        }
        else if traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .compact  {
            designLabel.font = labelView.font.withSize(fontSize_cc)
            designLabel.minimumScaleFactor = minScale_cc
        }
        else {
            designLabel.font = labelView.font.withSize(fontSize)
            designLabel.minimumScaleFactor = minScale
        }
        
        return (font:designLabel.font, minimumScaleFactor:designLabel.minimumScaleFactor)
    }
    
    private func updateLabelFontSize() {
        
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular {
            labelView.font = labelView.font.withSize(fontSize_rr)
            labelView.minimumScaleFactor = minScale_rr
        }
        else if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .compact {
            labelView.font = labelView.font.withSize(fontSize_rc)
            labelView.minimumScaleFactor = minScale_rc
        }
        else if traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .regular {
            labelView.font = labelView.font.withSize(fontSize_cr)
            labelView.minimumScaleFactor = minScale_cr
        }
        else if traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .compact  {
            labelView.font = labelView.font.withSize(fontSize_cc)
            labelView.minimumScaleFactor = minScale_cc
        }
        else {
            labelView.font = labelView.font.withSize(fontSize)
            labelView.minimumScaleFactor = minScale
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if ((self.traitCollection.verticalSizeClass != previousTraitCollection?.verticalSizeClass)
            || (self.traitCollection.horizontalSizeClass != previousTraitCollection?.horizontalSizeClass)) {
            updateLabelFontSize()
            setNeedsLayout()
        }
    }
    
    private func initialSetup() {
        
        //translatesAutoresizingMaskIntoConstraints = false
        
        // scrollView
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        //       scrollView.backgroundColor = UIColor.brown
        
        addSubview(scrollView)
        
        scrollView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        // content
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        //        contentView.backgroundColor = UIColor.blue
        
        scrollView.addSubview(contentView)
        
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        // label
        labelView.translatesAutoresizingMaskIntoConstraints = false
        //labelView.backgroundColor = UIColor.orange
        
        contentView.addSubview(labelView)
        
        // leading, trailing, bottom only. NOT top
        labelView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        labelView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        labelView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        // contentView height: no vertical scrolling. set contentHeight = ourHeight
        //contentView(equalTo: heightAnchor, multiplier: 1.0, constant: 0).isActive = true
        
        // content width
        contentWidthConstraint = contentView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1.0, constant: 0)
        contentWidthConstraint.priority = UILayoutPriority(rawValue: 800)
        contentWidthConstraint.isActive = true
        
        setNeedsLayout()
    }
    
    private func initializeProperties() {
        //self.text = "Hello Abby! Would you like to go to the park?"
        //self.text = "Hello Abby! Bark! bark?"
        //  self.backgroundColor = UIColor.clear
        labelView.text = self.text
        //   labelView.backgroundColor = UIColor.orange
        labelView.textColor = UIColor.white
        labelView.font = UIFont.systemFont(ofSize: 60)
        labelView.textAlignment = .right
        labelView.contentMode = .right
        labelView.baselineAdjustment = .alignBaselines
        labelView.adjustsFontSizeToFitWidth = true
        labelView.numberOfLines = 1
        labelView.minimumScaleFactor = 0.58
    }
    
    private func resizeContent() {
        let scaleFactor = labelView.minimumScaleFactor != 0 ? labelView.minimumScaleFactor : 1.0
        let smallestPointSize = labelView.font.pointSize * scaleFactor
        //smallestPointSize = 40.0
        let refLabel = UILabel()
        refLabel.font = labelView.font.withSize(smallestPointSize)
        
        var smallestWidth:CGFloat = 0
        if _setWithAttributedText {
            refLabel.attributedText = labelView.attributedText
            smallestWidth = labelView.attributedText?.size().width ?? 0
            print("resizeContent attributedText width: \(smallestWidth) string: \(String(describing: labelView.attributedText?.string))")
            
            print("resizeContent text estWidth: \(contentSize.width)")
        }
        else {
            refLabel.text = labelView.text
            smallestWidth = refLabel.intrinsicContentSize.width
            //print("resizeContent text smWidth: \(smallestWidth) string: \(String(describing: labelView.text))")
        }
        
//        let smallestWidth = refLabel.intrinsicContentSize.width
  //      let smallestWidth = labelView.attributedText?.size().width ?? 0
        
        var xOffset:CGFloat = 0
        
        if smallestWidth <= self.bounds.width {
            // no scrolling, FITS with smallest font
            self.contentWidthConstraint.constant = 0
            xOffset = 0
        }
        else {
            // scrolling needed. DOES NOT FIT
            let dW = smallestWidth - self.bounds.width
            self.contentWidthConstraint.constant = dW
            xOffset = dW
        }
        
        let offset = CGPoint(x: xOffset + scrollView.contentInset.right, y: 0)
        
        scrollView.setContentOffset(offset, animated: false)
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
