//
//  Array+isNumber.swift
//  Calc
//
//  Created by stephen feggeler on 8/26/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

extension Array where Iterator.Element == String {
    
    var isLastNumber : Bool {
        
        if let last = self.last, last.isNumber {
            return true
        }
        
        return false
    }
    
    var isLastZero : Bool {
        
        if isLastNumber {
            if 0.0 == Double(last!) {
                return true
            }            
        }
        
        return false
    }
    
    func isLastNumber(alsoNumbers:[String]) -> Bool {
        
        if let last = self.last, alsoNumbers.contains(last) {
            return true
        }
        
        return isLastNumber
    }
    
}

extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}

