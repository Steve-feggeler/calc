//
//  CalcNotes.swift
//  Calc
//
//  Created by stephen feggeler on 1/18/18.
//  Copyright © 2018 stephen feggeler. All rights reserved.
//

import UIKit

enum ReversePolishNotation: Error {
    case parenthesisMismatch
    case operationMissingNumber
    case unknownError
}

precedencegroup Exponentiative {
    associativity: left
    higherThan: MultiplicationPrecedence
}

precedencegroup DotProductPrecedence {
    lowerThan: AdditionPrecedence
    associativity: left
}

infix operator •: DotProductPrecedence
infix operator +-: AdditionPrecedence

protocol OperatorType {
    func description() -> String
    func precedence() -> Int
    func Associativity() -> String
}

enum MathOperation {
    case add, subtract, multiply, divide
    case number(Double)
}
