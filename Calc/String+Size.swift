//
//  String+Size.swift
//  Calc
//
//  Created by stephen feggeler on 7/28/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
    func width(withConstraintedHeight height: CGFloat, constraintedWidth:CGFloat, font: UIFont, minimumScaleFactor:CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        
        let boundingBoxLargeFont = self.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedStringKey.font: font],
            context: nil)
        
        // scale never zero
        let scale = minimumScaleFactor != 0 ? minimumScaleFactor: 1.0
        let smallestFont = font.withSize(font.pointSize * scale)
        
        let boundingBoxSmallFont = self.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedStringKey.font: smallestFont],
            context: nil)
        
        // if biggest text fits in constraintedWidth, use largest font for width
        if constraintedWidth >= boundingBoxLargeFont.width {
            return ceil(boundingBoxLargeFont.width)
        }
        // if smallest text bigger than constraintedWidth, use smallest font for width
        else if constraintedWidth <= boundingBoxSmallFont.width {
            return ceil(boundingBoxSmallFont.width)
        }
        else {
            // font shrinks to fit text until it is too long.
            return constraintedWidth
        }
    }
    
    func bounds(withConstraintedHeight height: CGFloat, constraintedWidth:CGFloat, font: UIFont, minimumScaleFactor:CGFloat) -> CGRect {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        
        var boundingBoxLargeFont = self.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedStringKey.font: font],
            context: nil)
        
        // scale never zero
        let scale = minimumScaleFactor != 0 ? minimumScaleFactor: 1.0
        let smallestFont = font.withSize(font.pointSize * scale)
        
        let boundingBoxSmallFont = self.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedStringKey.font: smallestFont],
            context: nil)
        
        // if biggest text fits in constraintedWidth, use largest font for width
        if constraintedWidth >= boundingBoxLargeFont.width {
            if constraintedWidth <= 736 {
                boundingBoxLargeFont.size.width += 1
                boundingBoxLargeFont.size.height += 5
            }
            else {
                boundingBoxLargeFont.size.width += -1
                boundingBoxLargeFont.size.height += -2
            }
            return boundingBoxLargeFont
        }
            
        // if smallest text bigger than constraintedWidth, use smallest font for width
        else if constraintedWidth <= boundingBoxSmallFont.width {
            return boundingBoxSmallFont
        }
            
        // font shrinks to fit text until it is too long.
        else {
            
            let percent = (constraintedWidth - boundingBoxSmallFont.width) / boundingBoxSmallFont.width + 1.0
            var bounds = CGRect(x: 0, y: 0, width: constraintedWidth, height: boundingBoxSmallFont.height * percent)
            
            bounds.size.width -= 10
            bounds.size.height -= 0
            
            return bounds
        }
    }
}
