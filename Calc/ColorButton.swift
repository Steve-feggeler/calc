//
//  ColorButton.swift
//  Calc
//
//  Created by stephen feggeler on 5/30/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

@IBDesignable

class ColorButton: UIButton {
    
    var borderLayers:[CALayer] = []
    
    @IBInspectable var edgeWidth: CGFloat = 1.5
    @IBInspectable var topEdge: Bool = false
    @IBInspectable var leftEdge: Bool = false
    @IBInspectable var bottomEdge: Bool = false
    @IBInspectable var rightEdge: Bool = false
    @IBInspectable var highlightColor: UIColor? {
        didSet {            
            let image = imageWithColor(highlightColor)
            setBackgroundImage(image, for: .highlighted)
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
 
    
    var isPending:Bool = false {
        didSet {
            if isPending {
                if topEdge {
                    addBorder(edge: .top, color: UIColor.darkText, thickness: edgeWidth)
                }
                if leftEdge {
                    addBorder(edge: .left, color: UIColor.darkText, thickness: edgeWidth)
                }
                if bottomEdge {
                    addBorder(edge: .bottom, color: UIColor.darkText, thickness: edgeWidth)
                }
                if rightEdge {
                    addBorder(edge: .right, color: UIColor.darkText, thickness: edgeWidth)
                }
            }
            else {
                for layer in borderLayers {
                    layer.removeFromSuperlayer()
                }
                borderLayers.removeAll()
            }
        }
    }

    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor
        
        self.layer.addSublayer(border)
        
        borderLayers.append(border)
    }

    override var backgroundColor: UIColor? {
        didSet {
            let image = imageWithColor(backgroundColor)
            setBackgroundImage(image, for: .normal)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.isPending {
            isPending = false
            isPending = true
        }
        setup()
    }
    
    
    override func awakeFromNib() {
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
    }
    
    func setup() {
        //let image = imageWithColor(highlightColor)
        //setBackgroundImage(image, for: .highlighted)
        
        /*
        let r = min(self.bounds.width, self.bounds.height) / 2.0
        cornerRadius = r * 0.6
        borderWidth = r * 0.15
        borderColor = #colorLiteral(red: 0.08341478556, green: 0.0834178254, blue: 0.08341617137, alpha: 1)
         */
    }
    
    
    private func imageWithColor(_ color:UIColor?) -> UIImage?
    {
        guard let color = color else { return nil }
        
        let size = bounds.size
        let radius = self.layer.cornerRadius
        let imageRect = CGRect(origin: CGPoint.zero, size: size)
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        let path = UIBezierPath(roundedRect: imageRect, cornerRadius: radius)
        
        color.set()
        
        path.fill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        //print("before image.size = \(String(describing: image?.size))")
        
        /*
        if radius > 0 {
            image = image?.resizableImage(withCapInsets:
                UIEdgeInsets(top: radius, left: radius, bottom: radius, right: radius)
            )
        }
        */
        
        //print("after image.size = \(String(describing: image?.size))")
        
        UIGraphicsEndImageContext()
        
        return image
    }
    
    private func imageAndLines() -> UIImage? {
        return nil
    }
    
    private func imageWithColorAndEdgeLines(_ color:UIColor?) -> UIImage?
    {
        guard let color = color else { return nil }
        
        let size = bounds.size
        let radius = self.layer.cornerRadius
        let imageRect = CGRect(origin: CGPoint.zero, size: size)
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        let path = UIBezierPath(roundedRect: imageRect, cornerRadius: radius)
        
        color.set()
        
        path.fill()
        
        // draw lines
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: size.width))
        path.lineWidth = 2
        UIColor.black.set()
        UIColor.green.setFill()
        path.stroke()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        //print("before image.size = \(String(describing: image?.size))")
        
        UIGraphicsEndImageContext()
        
        return image
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
