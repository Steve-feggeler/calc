//
//  Double+formated.swift
//  Calc
//
//  Created by stephen feggeler on 8/14/17.
//  Copyright © 2017 stephen feggeler. All rights reserved.
//

import UIKit

extension Double {
    
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func calculatorStyle(maxDigits:Int, maxFractionDigits:Int) -> String {
        
        var maxDecimalString = ""
        var maxDecimal:Double = 999999999999
        
        // make max number
        for _ in 1...maxDigits {
            maxDecimalString += "9"
        }
        
        if let d = Double(maxDecimalString) {
            maxDecimal = d
        }
        
        
        var positiveFormat = "0."
        for _ in 1...(maxDigits-4){
            positiveFormat += "#"
        }
        positiveFormat += "E0"
        
        
        let formatter = NumberFormatter()
        
        if abs(self) > maxDecimal {
            formatter.numberStyle = .scientific
            //formatter.positiveFormat = "0.######E0"
            formatter.positiveFormat = positiveFormat
            //formatter.negativeFormat = ""
            formatter.exponentSymbol = "e"
        }
        else if abs(self) > 0 && abs(self) < 0.0000000001 {
            formatter.numberStyle = .scientific
            //formatter.positiveFormat = "0.######E0"
            formatter.positiveFormat = positiveFormat
            //formatter.negativeFormat = ""
            formatter.exponentSymbol = "e"
        }
        else {
            let ourDigitsCount = digitsCount
            let maxFractionDigits = max(maxDigits - ourDigitsCount.integerDigitsCount, 0)
            
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = maxFractionDigits
            formatter.roundingMode = .down
        }
        
        let formattedNumber = formatter.string(from: NSNumber(value:self))
        return formattedNumber ?? description
    }


    var calculatorStyle1: String {
        let formatter = NumberFormatter()
        if self > 999999999999 {
            formatter.numberStyle = .scientific
            formatter.positiveFormat = "0.######E0"
            formatter.exponentSymbol = "e"
        }
        else {
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 7
            formatter.roundingMode = .down
        }
        let formattedNumber = formatter.string(from: NSNumber(value:self))
        return formattedNumber ?? description
    }
    
    var calculatorStyle2: String {
        let formatter = NumberFormatter()
        if abs(self) > 999999999999 {
            formatter.numberStyle = .scientific
            formatter.positiveFormat = "0.######E0"
            //formatter.negativeFormat = ""
            formatter.exponentSymbol = "e"
        }
        else if abs(self) > 0 && abs(self) < 0.0000000001 {
            formatter.numberStyle = .scientific
            formatter.positiveFormat = "0.######E0"
            //formatter.negativeFormat = ""
            formatter.exponentSymbol = "e"
        }
        else {
            let ourDigitsCount = digitsCount
            let maxFractionDigits = max(12 - ourDigitsCount.integerDigitsCount, 0)
            
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = maxFractionDigits
            formatter.roundingMode = .down
        }
        
        let formattedNumber = formatter.string(from: NSNumber(value:self))
        return formattedNumber ?? description
    }
    
    var digitsCount:(integerDigitsCount:Int, frationalDigitsCount:Int) {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 12
        guard var numberString = nf.string(from: NSNumber(value: self)) else {
            return (0, 0)
        }
        numberString = numberString.replacingOccurrences(of: ",", with: "")
        let components = numberString.components(separatedBy: ".")
        var integerDigitsCount = 0
        var fractionalDigitsCount = 0
        for (index, comp) in components.enumerated() {
            if index == 0 {
                integerDigitsCount = comp.count
            }
            else if index == 1 {
                fractionalDigitsCount = comp.count
            }
        }
        return (integerDigitsCount, fractionalDigitsCount)
    }
    
}
