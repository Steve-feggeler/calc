//
//  Stack.swift
//  Calc
//
//  Created by stephen feggeler on 1/18/18.
//  Copyright © 2018 stephen feggeler. All rights reserved.
//

import UIKit

struct Stack<Element> {
    
    var items = [Element]()
    
    mutating func push(_ item: Element) { items.append(item) }
    mutating func pop() -> Element { return items.removeLast() }
    
    func peek() -> Element? { return items.last }
    
    var count:Int { return items.count }
    var top:Element? { return items.last }
    var isEmpty:Bool { return items.count == 0 }
}
