//
//  Operator.swift
//  Calc
//
//  Created by stephen feggeler on 1/18/18.
//  Copyright © 2018 stephen feggeler. All rights reserved.
//

import UIKit

struct Operator {
    
    enum associativity:Int { case none, left, right }
    
    let associativity:associativity
    let precedence:Int
    let symbol:String
    let unary:Bool
    let isFunction:Bool
    
    init(symbol:String, precedence:Int, associativity:associativity, unary:Bool = false, isFunction:Bool = false) {
        self.symbol = symbol
        self.precedence = precedence
        self.associativity = associativity
        self.unary = unary
        self.isFunction = isFunction
    }
}
